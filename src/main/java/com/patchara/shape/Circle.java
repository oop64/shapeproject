/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shape;

/**
 *
 * @author user1
 */
public class Circle extends Shape{
    private double r;
    private double pi = 22.0/7;

    public Circle(double r) {
        super();
        System.out.println("Circle create");
        this.r = r;
    }
    
    @Override
    public double calArea() {
        return pi*r*r;
    }

    public void print() {
        System.out.println("Circle : r : "+r+", arear = "+this.calArea());
    }
}
