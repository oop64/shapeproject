/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shape;

/**
 *
 * @author user1
 */
public class Triangle extends Shape{
    private double base,height;
    
    public Triangle(double base, double height) {
        super();
        System.out.println("Triangle create");
        this.base = base;
        this.height = height;
    }
    
    @Override
    public double calArea() {
        return 0.5*base*height;
    }

    public void print() {
        System.out.println("Triangle : base : "+base+" height : "+height+", arear = "+this.calArea());
    }
}
