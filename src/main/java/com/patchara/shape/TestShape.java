/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shape;

/**
 *
 * @author user1
 */
public class TestShape {
    public static void main(String[] args) {
        Circle circle = new Circle(3);
        circle.print();
        newLine();
        
        Triangle triangle =  new Triangle(2,4);
        triangle.print();
        newLine();
        
        Rectangle rectangle = new Rectangle(4,6);
        rectangle.print();
        newLine();
        
        Square square = new Square(4);
        square.print();
        newLine();
    }

    private static void newLine() {
        System.out.println("");
    }  
}
