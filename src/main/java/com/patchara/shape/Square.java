/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.patchara.shape;

/**
 *
 * @author user1
 */
public class Square extends Rectangle {

    private double side = width;

    public Square(double side) {
        super(side, side);
        System.out.println("Square create");
        this.side = side;
    }

    @Override
    public double calArea() {
        return side * side;
    }

    @Override
    public void print() {
        System.out.println("Square : side : " + side + ", arear = " + this.calArea());
    }
}
